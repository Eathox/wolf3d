/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   wolf3d.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/25 13:07:13 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 14:24:24 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_WOLF3D_H
# define FT_WOLF3D_H

# include "libft/typedefs.h"
# define TILE_SPACING	60

# ifdef linux
#  define VLC_ADDRESS "/usr/bin/vlc/vlc"
# else
#  define VLC_ADDRESS "/Applications/VLC.app/Contents/MacOS/VLC"
# endif

typedef enum	e_tileuid
{
	TILE_TERM = -1,
	FLOOR,
	WALL,
	WALL_A,
	WALL_B,
	WALL_C,
	WALL_D,
	WALL_E,
	GLASS,
	TILE_RANGE
}				t_tileuid;

typedef enum	e_objuid
{
	OBJ_TERM = -1,
	BARRLE,
	CRATE,
	CHAIR,
	TABLE,
	SPAWN,
	SKYBOX,
	OBJ_RANGE
}				t_objuid;

typedef struct	s_vec3
{
	float	x;
	float	y;
	float	z;
}				t_vec3;

typedef struct	s_tile
{
	t_tileuid	uid;
	t_vec3		point[4];
}				t_tile;

typedef struct	s_object
{
	t_objuid	uid;
	t_vec3		point;
}				t_object;

typedef struct	s_tga
{
	unsigned char	idlen;
	unsigned char	clr_map_type;
	unsigned char	img_type;
	unsigned short	cmpos;
	unsigned short	cmlen;
	unsigned short	cmsize;
}				t_tga;

typedef struct	s_img
{
	unsigned char	pxdepth;
	unsigned short	xorigin;
	unsigned short	yorigin;
	unsigned short	width;
	unsigned short	height;
	unsigned int	*pixels;
}				t_img;

typedef struct	s_level
{
	t_vec3		spawn;
	t_tile		**terrain;
	t_object	**objects;
	size_t		xsize;
	size_t		ysize;
	t_img		*skybox;
	t_img		*floor;
	t_img		*ceiling;
	char		*music;
}				t_level;

typedef struct	s_hit
{
	t_vec3		pos;
	t_vec3		line[2];
	t_tileuid	uid;
}				t_hit;

t_bool			ft_levelsetimg(char *file, t_img **img);
t_bool			ft_readobjects(t_level *level, int fd);
t_bool			ft_readterrain(t_level *level, int fd);
t_bool			ft_validateline(char **split, size_t len, char *chrs);
t_level			*ft_levelnew(char *file);
t_tileuid		**ft_terrainnew(size_t xsize, size_t ysize);
void			**ft_lsttoarr(t_list *lst);
void			ft_leveldel(t_level **level);
void			ft_objectsdel(t_object ***objects);
void			ft_putobjectinfo(t_object *obj);
void			ft_puttileinfo(t_tile *tile);
void			ft_terraindel(t_tile ***terrain);

#endif
