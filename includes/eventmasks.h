/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   eventmasks.h                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/04/03 13:18:20 by pholster       #+#    #+#                */
/*   Updated: 2019/07/22 20:26:19 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_EVENTMASKS_H
# define FT_EVENTMASKS_H

/*
** WINDOW
*/

# define EVENT_TYPE_WINDOW_CLOSED	17
# define EVENT_TYPE_WINDOW_EXPOSE	12
# define EVENT_MASK_WINDOW_CLOSED	0
# define EVENT_MASK_WINDOW_EXPOSE	(1L << 12)

/*
** MOUSE
*/

# define EVENT_TYPE_MOUSE_DOWN	4
# define EVENT_TYPE_MOUSE_UP	5
# define EVENT_TYPE_MOUSE_MOVE	6
# define EVENT_MASK_MOUSE_DOWN	(1L << 2)
# define EVENT_MASK_MOUSE_UP	12
# define EVENT_MASK_MOUSE_MOVE	(1L << 6)

/*
** KEYBOARD
*/

# define EVENT_TYPE_KEY_DOWN	2
# define EVENT_TYPE_KEY_UP		3
# define EVENT_MASK_KEY_DOWN	(1L << 0)
# define EVENT_MASK_KEY_UP		(1L << 1)

#endif
