/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   game.h                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 17:30:00 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/22 14:58:26 by ehollidg      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GAME_H
# define FT_GAME_H

# include "wolf3d.h"
# include "libft/threadpool.h"
# include <signal.h>

# define POOL_TIME		FALSE
# define POOL_SIZE		2
# define SCREEN_HEIGHT	720
# define SCREEN_WIDTH	1280
# define HALF_SCREEN_H	(SCREEN_HEIGHT / 2)
# define HALF_SCREEN_W	(SCREEN_WIDTH / 2)
# define PI				3.14159265359
# define PI_RADIANS		0.0174532925
# define FOV			60
# define FOV2			(FOV / 2)
# define RAYCOUNT		SCREEN_WIDTH
# define FADEINTESITY	110
# define TURN_SPEED		2.6
# define MOVE_SPEED		3
# define STRAFE_RATIO	0.8
# define RUN_RATIO		1.6
# define COLLISION_DIST 10
# define DEFAULT_MUSIC	"audio/bg_music.mp3"

typedef struct	s_game
{
	void	*mlx;
	void	*win;
	void	*img;
	void	*img_add;
	t_level	*level;
	float	angle;
	t_vec3	position;
	t_img	*ttext[7];
	long	helddown;
	t_bool	redraw;
	t_pool	*pool;
	pid_t	forks[2];
}				t_game;

void			ft_playermove(t_game *game);
void			ft_game(t_level *level);
void			ft_plot(int x, int y, t_color colour, void *img_add);
int				ft_hookcontrols(t_game *game);
int				ft_eventmousedown(int code, int x, int y, t_game *game);
int				ft_eventmouseup(int code, int x, int y, t_game *game);
int				ft_eventkeydown(int code, t_game *game);
int				ft_eventkeyup(int code, t_game *game);
float			ft_dist(t_vec3 *v0, t_vec3 *v1);
t_bool			ft_closer(t_vec3 *v0, t_vec3 *v1, t_vec3 *pos);
void			ft_drawrect(t_vec3 *v, float fade, t_game *game, t_hit *hit);
void			ft_draw3dview(t_game *game, t_hit *hits);
void			ft_traceray(t_vec3 *angles, t_game *game, t_hit *hit);
void			ft_tracerays(t_hit *hits, t_game *game);
int				ft_loop(t_game *game);
int				ft_tgapfp(unsigned char *str, size_t i, t_img *img);
t_img			*ft_readtga(char *file);
void			ft_tgadel(t_img **img);
void			ft_drawskybox(t_img *skybox, t_game *game);
t_color			ft_getcolour(float fade, t_game *game, t_vec3 *v, t_hit *hit);
int				ft_doescollide(t_vec3 *pos, t_game *game);
void			ft_drawglass(t_vec3 *pos, t_hit *hit, float angle,
					t_game *game);
t_vec3			*ft_getcollision(t_game *game, t_vec3 *a, t_vec3 *wall,
					t_vec3 *pos);
void			ft_drawceilingfloor(float *fts, t_game *game, t_vec3 *pos,
					t_hit *hit);

#endif
