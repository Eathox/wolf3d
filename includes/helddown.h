/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   helddown.h                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/04/03 13:18:20 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 16:40:02 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HELDDOWN_H
# define FT_HELDDOWN_H

# define HELDDOWN_MOUSE_LEFT		(1L << 0)
# define HELDDOWN_MOUSE_RIGHT		(1L << 1)
# define HELDDOWN_KEY_SHIFT			(1L << 2)
# define HELDDOWN_KEY_CTRL			(1L << 3)
# define HELDDOWN_KEY_ALT			(1L << 4)
# define HELDDOWN_KEY_WIN			(1L << 5)
# define HELDDOWN_KEY_Q				(1L << 6)
# define HELDDOWN_KEY_W				(1L << 7)
# define HELDDOWN_KEY_E				(1L << 8)
# define HELDDOWN_KEY_A				(1L << 9)
# define HELDDOWN_KEY_S				(1L << 10)
# define HELDDOWN_KEY_D				(1L << 11)
# define HELDDOWN_KEY_F				(1L << 12)
# define HELDDOWN_KEY_SPACE			(1L << 13)

#endif
