/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   keycodes_linux.h                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/04/03 13:18:20 by pholster       #+#    #+#                */
/*   Updated: 2019/07/22 20:33:45 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_KEYCODES_LINUX_H
# define FT_KEYCODES_LINUX_H

/*
** MOUSE
*/

# define MOUSE_LEFT			1
# define MOUSE_RIGHT		3
# define MOUSE_MIDDLE		2
# define MOUSE_SCROLLUP		4
# define MOUSE_SCROLLDOWN	5
# define MOUSE_SCROLLLEFT	6
# define MOUSE_SCROLLRIGHT	7

/*
** KEYBOARD
*/

/*
** Might work, might not work
** # define KEY_NUMLOCK		71 //TODO
** # define KEY_SCROLLLOCK	107 //TODO
** # define KEY_NUMPADEQUAL	81 //TODO
** # define KEY_CAPSLOCK	272 //TODO
*/

# define KEY_F1				65470
# define KEY_F2				65471
# define KEY_F3				65472
# define KEY_F4				65473
# define KEY_F5				65474
# define KEY_F6				65475
# define KEY_F7				65476
# define KEY_F8				65477
# define KEY_F9				65478
# define KEY_F10			65479
# define KEY_F11			65480
# define KEY_F12			65481
# define KEY_F13			65482
# define KEY_F14			65483
# define KEY_F15			65484
# define KEY_F16			65485
# define KEY_F17			65486
# define KEY_F18			65487
# define KEY_F19			65488
# define KEY_0				48
# define KEY_1				49
# define KEY_2				50
# define KEY_3				51
# define KEY_4				52
# define KEY_5				53
# define KEY_6				54
# define KEY_7				55
# define KEY_8				56
# define KEY_9				57
# define KEY_NUMPAD0		65438
# define KEY_NUMPAD1		65436
# define KEY_NUMPAD2		65433
# define KEY_NUMPAD3		65435
# define KEY_NUMPAD4		65430
# define KEY_NUMPAD5		65437
# define KEY_NUMPAD6		65432
# define KEY_NUMPAD7		65429
# define KEY_NUMPAD8		65431
# define KEY_NUMPAD9		65434
# define KEY_A				97
# define KEY_B				98
# define KEY_C				99
# define KEY_D				100
# define KEY_E				101
# define KEY_F				102
# define KEY_G				103
# define KEY_H				104
# define KEY_I				105
# define KEY_J				106
# define KEY_K				107
# define KEY_L				108
# define KEY_M				109
# define KEY_N				110
# define KEY_O				111
# define KEY_P				112
# define KEY_Q				113
# define KEY_R				114
# define KEY_S				115
# define KEY_T				116
# define KEY_U				117
# define KEY_V				118
# define KEY_W				119
# define KEY_X				120
# define KEY_Y				121
# define KEY_Z				122
# define KEY_ESCAPE			65307
# define KEY_TAB			65289
# define KEY_LSHIFT			65505
# define KEY_RSHIFT			65506
# define KEY_LCONTROL		65507
# define KEY_RCONTROL		65508
# define KEY_BACKSPACE		65288
# define KEY_ENTER			65293
# define KEY_NUMPADENTER	65421
# define KEY_LALT			65513
# define KEY_RALT			65514
# define KEY_SPACE			32
# define KEY_MINUS			45
# define KEY_EQUALS			61
# define KEY_LBRACKET		91
# define KEY_RBRACKET		93
# define KEY_SEMICOLON		59
# define KEY_APOSTROPHE		39
# define KEY_GRAVE			96
# define KEY_BACKSLASH		47
# define KEY_COMMA			44
# define KEY_PERIOD			46
# define KEY_SLASH			92
# define KEY_NUMPADSTAR		65450
# define KEY_NUMPADMINUS	65453
# define KEY_NUMPADPLUS		65451
# define KEY_NUMPADPERIOD	65439
# define KEY_NUMPADSLASH	65455
# define KEY_INSERT			65379
# define KEY_HOME			65360
# define KEY_UPARROW		65362
# define KEY_PGUP			65365
# define KEY_LEFTARROW		65361
# define KEY_RIGHTARROW		65363
# define KEY_END			65367
# define KEY_DOWNARROW		65364
# define KEY_PGDN			65366
# define KEY_DELETE			65535
# define KEY_LWIN			65515
# define KEY_RWIN			65516

#endif
