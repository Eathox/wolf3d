/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   keycodes.h                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/04/03 13:18:20 by pholster       #+#    #+#                */
/*   Updated: 2019/07/22 20:26:31 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_KEYCODES_H
# define FT_KEYCODES_H

# ifdef linux
#  include "keycodes_linux.h"
# else
#  include "keycodes_macos.h"
# endif

#endif
