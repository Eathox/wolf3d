/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_getcollision.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 18:56:57 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/09 11:51:08 by ehollidg      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

t_vec3		*ft_getcollision(t_game *game, t_vec3 *a, t_vec3 *wall, t_vec3 *pos)
{
	float	x;
	float	y;
	float	d;
	t_vec3	vec;

	x = game->position.x + a->x;
	y = game->position.y + a->y;
	d = ((wall[0].x - wall[1].x) * (game->position.y - y)) -
		((wall[0].y - wall[1].y) * (game->position.x - x));
	if (d == 0)
		return (NULL);
	vec.x = (((wall[0].x - game->position.x) * (game->position.y - y)) -
			((wall[0].y - game->position.y) * (game->position.x - x))) / d;
	vec.y = -(((wall[0].x - wall[1].x) * (wall[0].y - game->position.y)) -
			((wall[0].y - wall[1].y) * (wall[0].x - game->position.x))) / d;
	if (vec.x > 0 && vec.x < 1 && vec.y > 0)
	{
		pos->x = wall[0].x + (vec.x * (wall[1].x - wall[0].x));
		pos->y = wall[0].y + (vec.x * (wall[1].y - wall[0].y));
		return (pos);
	}
	return (NULL);
}
