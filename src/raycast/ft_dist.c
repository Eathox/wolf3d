/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_dist.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/26 11:55:31 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/09 13:10:39 by ehollidg      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

float	ft_dist(t_vec3 *v0, t_vec3 *v1)
{
	float x;
	float y;

	x = v0->x - v1->x;
	x = x * x;
	y = v0->y - v1->y;
	y = y * y;
	return (sqrtf(x + y));
}
