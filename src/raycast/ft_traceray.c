/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_traceray.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 18:33:47 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:52:21 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"

static void	tohit(t_vec3 **v, t_vec3 *wall, t_hit *hit, t_tile *tile)
{
	ft_memcpy(v[1], v[0], sizeof(t_vec3));
	hit->line[0] = (t_vec3){wall[0].x, wall[0].y, wall[0].z};
	hit->line[1] = (t_vec3){wall[1].x, wall[1].y, wall[1].z};
	hit->uid = tile->uid;
}

static void	objcollide(t_game *game, t_vec3 **v, t_tile *tile, t_hit *hit)
{
	short	i;
	t_vec3	wall[2];

	i = 3;
	while (i >= 0)
	{
		wall[0] = tile->point[i];
		wall[1] = tile->point[(i + 1) % 4];
		ft_getcollision(game, v[2], wall, v[0]);
		if (ft_closer(v[0], v[1], &game->position) == TRUE)
			tohit(v, wall, hit, tile);
		i--;
	}
}

void		ft_traceray(t_vec3 *angles, t_game *game, t_hit *hit)
{
	size_t	i;
	t_vec3	vec;
	t_vec3	pos;
	t_vec3	*v[3];

	i = 0;
	vec = (t_vec3){2147483648, 2147483648, 2147483648};
	ft_memcpy(&pos, &vec, sizeof(vec));
	v[0] = &pos;
	v[1] = &vec;
	v[2] = angles;
	while (game->level->terrain[i] != NULL)
	{
		if (game->level->terrain[i]->uid != FLOOR)
			objcollide(game, v, game->level->terrain[i], hit);
		i++;
	}
	if (vec.x != -2147483648)
		ft_memcpy(&(hit->pos), &vec, sizeof(vec));
	else
		hit->pos = (t_vec3){game->position.x + (angles->x * 1000),
			game->position.y + (angles->y * 1000), 0};
}
