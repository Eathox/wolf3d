/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_closer.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/25 14:32:56 by ehollidg       #+#    #+#                */
/*   Updated: 2019/07/24 17:03:20 by ehollidg      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

t_bool		ft_closer(t_vec3 *v0, t_vec3 *v1, t_vec3 *pos)
{
	return (ft_dist(v0, pos) < ft_dist(v1, pos));
}
