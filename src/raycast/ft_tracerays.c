/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_tracerays.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 19:45:40 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/16 15:39:32 by ehollidg      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include <math.h>

void		ft_tracerays(t_hit *hits, t_game *game)
{
	float	step;
	float	angle_cos;
	float	angle_sin;
	size_t	i;
	float	stepsize;

	i = 0;
	step = 0;
	stepsize = ((float)FOV / RAYCOUNT);
	while (i < RAYCOUNT)
	{
		angle_cos = cos((float)(step + game->angle) * PI_RADIANS);
		angle_sin = sin((float)(step + game->angle) * PI_RADIANS);
		ft_traceray(&(t_vec3){angle_cos, angle_sin, 0}, game, &hits[i]);
		step += stepsize;
		i++;
	}
}
