/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lsttoarr.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/01 13:41:43 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:02:25 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/libft.h"

void	**ft_lsttoarr(t_list *lst)
{
	size_t	i;
	void	**arr;

	i = 0;
	arr = (void **)ft_memalloc(sizeof(void *) * (ft_lstlen(lst) + 1));
	if (arr == NULL)
		return (NULL);
	while (lst != NULL)
	{
		arr[i] = lst->content;
		lst = lst->next;
		i++;
	}
	return (arr);
}
