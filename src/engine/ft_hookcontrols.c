/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_hookcontrols.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/24 16:20:19 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 16:18:21 by ehollidg      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "eventmasks.h"
#include "mlx.h"

static int	exit1(t_game *game)
{
	kill(game->forks[0], SIGKILL);
	kill(game->forks[1], SIGKILL);
	exit(0);
}

int			ft_hookcontrols(t_game *game)
{
	mlx_hook(game->win, EVENT_TYPE_WINDOW_CLOSED, EVENT_MASK_WINDOW_CLOSED,
		&exit1, game);
	mlx_hook(game->win, EVENT_TYPE_MOUSE_DOWN, EVENT_MASK_MOUSE_DOWN,
		&ft_eventmousedown, game);
	mlx_hook(game->win, EVENT_TYPE_MOUSE_UP, EVENT_MASK_MOUSE_UP,
		&ft_eventmouseup, game);
	mlx_hook(game->win, EVENT_TYPE_KEY_DOWN, EVENT_MASK_KEY_DOWN,
		&ft_eventkeydown, game);
	mlx_hook(game->win, EVENT_TYPE_KEY_UP, EVENT_MASK_KEY_UP,
		&ft_eventkeyup, game);
	return (0);
}
