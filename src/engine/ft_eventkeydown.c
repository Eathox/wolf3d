/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_eventkeydown.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/22 20:24:08 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 16:40:07 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "keycodes.h"
#include "helddown.h"

static void	exitprogram(t_game *game)
{
	kill(game->forks[0], SIGKILL);
	kill(game->forks[1], SIGKILL);
	exit(0);
}

static void	movekeys(int code, t_game *game)
{
	if (code == KEY_Q)
		game->helddown |= HELDDOWN_KEY_Q;
	else if (code == KEY_W || code == KEY_UPARROW)
		game->helddown |= HELDDOWN_KEY_W;
	else if (code == KEY_E)
		game->helddown |= HELDDOWN_KEY_E;
	else if (code == KEY_A || code == KEY_LEFTARROW)
		game->helddown |= HELDDOWN_KEY_A;
	else if (code == KEY_S || code == KEY_DOWNARROW)
		game->helddown |= HELDDOWN_KEY_S;
	else if (code == KEY_D || code == KEY_RIGHTARROW)
		game->helddown |= HELDDOWN_KEY_D;
}

int			ft_eventkeydown(int code, t_game *game)
{
	if (code == KEY_ESCAPE)
		exitprogram(game);
	if (code == KEY_LSHIFT || code == KEY_RSHIFT)
		game->helddown |= HELDDOWN_KEY_SHIFT;
	else if (code == KEY_LCONTROL || code == KEY_RCONTROL)
		game->helddown |= HELDDOWN_KEY_CTRL;
	else if (code == KEY_LALT || code == KEY_RALT)
		game->helddown |= HELDDOWN_KEY_ALT;
	else if (code == KEY_LWIN || code == KEY_RWIN)
		game->helddown |= HELDDOWN_KEY_WIN;
	else if (code == KEY_F)
		game->helddown |= HELDDOWN_KEY_F;
	else if (code == KEY_SPACE)
		game->helddown |= HELDDOWN_KEY_SPACE;
	else
		movekeys(code, game);
	return (0);
}
