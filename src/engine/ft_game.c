/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_game.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 17:29:36 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/22 15:20:57 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "mlx.h"
#include "libft/libft.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

static t_bool	checkttext(t_game *game)
{
	int i;

	i = 0;
	while (i < 6)
	{
		if (game->ttext[i] == NULL)
		{
			ft_puterror("A Texture was not Found");
			return (FALSE);
		}
		i++;
	}
	return (TRUE);
}

static void		loadtextures(t_game *game)
{
	game->ttext[0] = ft_readtga("textures/bluewall.tga");
	game->ttext[1] = ft_readtga("textures/greenwall.tga");
	game->ttext[2] = ft_readtga("textures/redwall.tga");
	game->ttext[3] = ft_readtga("textures/yellowwall.tga");
	game->ttext[4] = ft_readtga("textures/wall1.tga");
	game->ttext[5] = ft_readtga("textures/wall.tga");
}

static void		playaudio(t_game *game)
{
	int		fd;
	char	*c;

	fd = open(VLC_ADDRESS, O_RDONLY);
	if (fd < 0 || read(fd, &c, 0) < 0)
	{
		close(fd);
		return ;
	}
	close(fd);
	game->forks[0] = fork();
	if (game->forks[0] == 0)
		execl(VLC_ADDRESS, "vlc", "audio/start.mp3", "vlc://quit",
			"-I", "dummy", "-V", "dummy", (char*)NULL);
	game->forks[1] = fork();
	if (game->forks[1] == 0)
		execl(VLC_ADDRESS, "vlc", game->level->music, "--play-and-exit", "-I",
			"dummy", "-V", "dummy", "--loop", "--gain=0.1", (char*)NULL);
}

void			ft_game(t_level *level)
{
	t_game	game;
	int		tmp;

	tmp = 0;
	game.mlx = mlx_init();
	game.win = mlx_new_window(game.mlx, SCREEN_WIDTH, SCREEN_HEIGHT, "Wolf3d");
	game.img = mlx_new_image(game.mlx, SCREEN_WIDTH, SCREEN_HEIGHT);
	game.img_add = mlx_get_data_addr(game.img, &tmp, &tmp, &tmp);
	loadtextures(&game);
	game.pool = ft_poolnew(POOL_SIZE, POOL_TIME);
	game.level = level;
	if (game.mlx == NULL || game.win == NULL || game.img == NULL ||
		game.img_add == NULL || checkttext(&game) == FALSE || game.pool == NULL)
		return ;
	game.angle = 0;
	ft_memcpy(&game.position, &level->spawn, sizeof(t_vec3));
	game.redraw = TRUE;
	ft_bzero(&game.helddown, sizeof(game.helddown));
	ft_hookcontrols(&game);
	mlx_loop_hook(game.mlx, ft_loop, &game);
	playaudio(&game);
	mlx_loop(game.mlx);
}
