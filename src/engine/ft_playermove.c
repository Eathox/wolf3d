/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_playermove.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/22 20:11:43 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 14:48:55 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "helddown.h"
#include "libft/libft.h"
#include <math.h>

static void		fswap(float *a, float *b)
{
	float	temp_a;

	temp_a = *a;
	*a = *b;
	*b = temp_a;
}

static t_bool	checknewpos(float xspeed, float yspeed, t_game *game)
{
	if (!ft_doescollide(&((t_vec3){game->position.x + xspeed,
			game->position.y + yspeed, 0}), game))
	{
		game->position.x += xspeed;
		game->position.y += yspeed;
		return (TRUE);
	}
	else if (!ft_doescollide(&((t_vec3){game->position.x + xspeed,
			game->position.y, 0}), game))
	{
		game->position.x += xspeed;
		return (TRUE);
	}
	else if (!ft_doescollide(&((t_vec3){game->position.x,
			game->position.y + yspeed, 0}), game))
	{
		game->position.y += yspeed;
		return (TRUE);
	}
	return (FALSE);
}

static t_bool	controlplayer(int code, t_game *game, t_bool shift)
{
	float	multi;
	float	xspeed;
	float	yspeed;

	if ((game->helddown & code) == FALSE)
		return (FALSE);
	multi = (shift == TRUE) ? RUN_RATIO : 1;
	xspeed = (MOVE_SPEED * multi) * cos((game->angle + FOV2) * PI_RADIANS);
	yspeed = (MOVE_SPEED * multi) * sin((game->angle + FOV2) * PI_RADIANS);
	if (code == HELDDOWN_KEY_S)
	{
		xspeed = -xspeed;
		yspeed = -yspeed;
	}
	else if (code == HELDDOWN_KEY_Q || code == HELDDOWN_KEY_E)
	{
		yspeed *= STRAFE_RATIO;
		xspeed *= STRAFE_RATIO;
		fswap(&yspeed, &xspeed);
		if (code == HELDDOWN_KEY_Q)
			yspeed = -yspeed;
		else
			xspeed = -xspeed;
	}
	return (checknewpos(xspeed, yspeed, game));
}

void			ft_playermove(t_game *game)
{
	t_bool	shift;
	t_bool	draw;

	draw = (game->helddown & HELDDOWN_KEY_A || game->helddown & HELDDOWN_KEY_D);
	if (game->helddown & HELDDOWN_KEY_A)
		game->angle -= TURN_SPEED;
	if (game->helddown & HELDDOWN_KEY_D)
		game->angle += TURN_SPEED;
	shift = (game->helddown & HELDDOWN_KEY_SHIFT) != 0;
	game->angle = ft_overflow(game->angle, 0, 360);
	draw |= controlplayer(HELDDOWN_KEY_W, game, shift);
	draw |= controlplayer(HELDDOWN_KEY_S, game, shift);
	draw |= controlplayer(HELDDOWN_KEY_E, game, shift);
	draw |= controlplayer(HELDDOWN_KEY_Q, game, shift);
	game->redraw |= draw;
	return ;
}
