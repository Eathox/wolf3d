/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_loop.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 18:18:30 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 13:36:54 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "mlx.h"
#include "libft/libft.h"
#include <time.h>

static void	putfps(t_game *game, float *timepassed, size_t *frames)
{
	static char	*fps = NULL;

	if (*timepassed >= 0.4)
	{
		ft_strdel(&fps);
		fps = ft_itoa(((float)*frames / *timepassed));
		*timepassed = 0;
		*frames = 0;
	}
	if (fps != NULL)
		mlx_string_put(game->mlx, game->win, 8, 10, 0xFFFFFF, fps);
}

static void	putpos(t_game *game)
{
	char	*tmp;
	char	str[12];

	str[0] = 'x';
	str[1] = ' ';
	tmp = ft_itoa(game->position.x);
	ft_strcpy(&str[2], tmp);
	ft_strdel(&tmp);
	mlx_string_put(game->mlx, game->win, 8, 30, 0xFFFFFF, str);
	str[0] = 'y';
	str[1] = ' ';
	tmp = ft_itoa(game->position.y);
	ft_strcpy(&str[2], tmp);
	ft_strdel(&tmp);
	mlx_string_put(game->mlx, game->win, 8, 50, 0xFFFFFF, str);
}

static void	putinfo(t_game *game, float *timepassed, size_t *frames)
{
	putfps(game, timepassed, frames);
	putpos(game);
}

int			ft_loop(t_game *game)
{
	static float	timepassed = 0;
	static size_t	frames = 0;
	struct timespec	start;
	struct timespec	end;
	t_hit			hits[RAYCOUNT];

	if (game->level == NULL)
		return (0);
	frames++;
	clock_gettime(CLOCK_REALTIME, &start);
	ft_playermove(game);
	if (game->redraw == TRUE)
	{
		game->redraw = FALSE;
		ft_tracerays(hits, game);
		ft_drawskybox(game->level->skybox, game);
		ft_draw3dview(game, hits);
	}
	mlx_put_image_to_window(game->mlx, game->win, game->img, 0, 0);
	clock_gettime(CLOCK_REALTIME, &end);
	if (end.tv_nsec > start.tv_nsec)
		timepassed += ((float)end.tv_nsec / 1000000000) -
			((float)start.tv_nsec / 1000000000);
	putinfo(game, &timepassed, &frames);
	return (0);
}
