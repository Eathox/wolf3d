/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_doescollide.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/12 10:53:02 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:59:15 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"
#include "libft/ft_printf.h"
#include <math.h>

static int		circlecol(t_vec3 *pos, t_vec3 *points)
{
	if (pos->y < points[0].y)
		return (ft_dist(pos, &((t_vec3){pos->x, points[0].y, 0}))
			< COLLISION_DIST);
	else if (pos->y > points[3].y)
		return (ft_dist(pos, &((t_vec3){pos->x, points[2].y, 0}))
			< COLLISION_DIST);
	else if (pos->x < points[0].x)
		return (ft_dist(pos, &((t_vec3){points[0].x, pos->y, 0}))
			< COLLISION_DIST);
	else
		return (ft_dist(pos, &((t_vec3){points[1].x, pos->y, 0}))
			< COLLISION_DIST);
}

int				ft_doescollide(t_vec3 *pos, t_game *game)
{
	int		i;
	t_tile	**terrain;

	i = 0;
	terrain = game->level->terrain;
	while (terrain[i])
	{
		if (pos->x - COLLISION_DIST < terrain[i]->point[1].x &&
			pos->x + COLLISION_DIST > terrain[i]->point[0].x &&
			pos->y - COLLISION_DIST < terrain[i]->point[2].y &&
			pos->y + COLLISION_DIST > terrain[i]->point[0].y &&
			terrain[i]->uid != FLOOR && circlecol(pos, terrain[i]->point))
			return (1);
		i++;
	}
	return (0);
}
