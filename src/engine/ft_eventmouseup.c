/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_eventmouseup.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/22 20:24:08 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:00:53 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "keycodes.h"
#include "helddown.h"

int		ft_eventmouseup(int code, int x, int y, t_game *game)
{
	if (code == MOUSE_LEFT)
		game->helddown ^= HELDDOWN_MOUSE_LEFT;
	else if (code == MOUSE_RIGHT)
		game->helddown ^= HELDDOWN_MOUSE_RIGHT;
	(void)x;
	(void)y;
	return (0);
}
