/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_drawceilingfloor.c                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 14:36:55 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 15:04:13 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"
#include <math.h>
#include <stdio.h>

static int	overflowcf(int i, int max)
{
	if (i < max)
		return (i);
	return (max - (i - max));
}

static void	drawcast(t_game *game, t_vec3 *vectors, int type)
{
	int		textx;
	int		texty;
	t_img	*img;
	float	floorvar;
	float	dist;

	if (type)
		img = game->level->ceiling;
	else
		img = game->level->floor;
	dist = (vectors[0].z);
	if (dist >= 1)
		dist = 1;
	floorvar = game->position.x + dist * (vectors[1].x - game->position.x);
	textx = (int)fabs(floorvar * 4) % img->width;
	floorvar = game->position.y + dist * (vectors[1].y - game->position.y);
	texty = (int)fabs(floorvar * 4) % img->height;
	ft_plot(vectors[0].x, vectors[0].y,
		img->pixels[(texty * img->width) + textx], game->img_add);
}

static void	drawcasting(t_vec3 *v, t_game *game, float *screendist, int type)
{
	short	x;
	short	y;
	t_vec3	vectors[2];
	float	dist;
	float	biggest;

	x = (short)v[1].x;
	biggest = fmax(screendist[overflowcf((short)v[1].y, HALF_SCREEN_H)],
		screendist[overflowcf((short)v[0].y, HALF_SCREEN_H)]);
	ft_memcpy(&vectors[1], &v[2], sizeof(t_vec3));
	while (x >= (short)v[0].x)
	{
		y = (short)v[1].y;
		while (y >= (short)v[0].y)
		{
			vectors[0].x = x;
			vectors[0].y = y;
			dist = screendist[overflowcf(y, HALF_SCREEN_H)] / biggest;
			vectors[0].z = dist;
			drawcast(game, vectors, type);
			y--;
		}
		x--;
	}
}

static void	setscreendist(float *screendist, float angle)
{
	int		i;
	float	step;
	float	totalstep;
	float	val;

	i = 0;
	val = 90;
	step = (float)45 / HALF_SCREEN_H;
	totalstep = 45;
	while (i < HALF_SCREEN_H)
	{
		screendist[i] = (val * tan(totalstep * PI_RADIANS)) *
			 cosf(angle * PI_RADIANS);
		i++;
		totalstep += step;
	}
}

void		ft_drawceilingfloor(float *fts, t_game *game, t_vec3 *pos,
	t_hit *hit)
{
	static float	screendist[HALF_SCREEN_H] = {-1};
	t_vec3			v[3];

	if (screendist[0] == -1)
		setscreendist(screendist, fts[1]);
	ft_memcpy(&v[2], &(hit->pos), sizeof(t_vec3));
	v[2].z = ft_dist(&(game->position), &hit->pos) * cosf(fts[1] * PI_RADIANS);
	if (game->level->floor != NULL)
	{
		ft_memcpy(&v[1], &((t_vec3){(int)(pos->x + pos->y), SCREEN_HEIGHT,
			fts[1]}), sizeof(t_vec3));
		ft_memcpy(&v[0], &((t_vec3){(int)pos->x, SCREEN_HEIGHT - fts[0], 0}),
			sizeof(t_vec3));
		drawcasting(v, game, screendist, 0);
	}
	if (game->level->ceiling != NULL)
	{
		ft_memcpy(&v[0], &((t_vec3){(int)pos->x, 0, 0}), sizeof(t_vec3));
		ft_memcpy(&v[1], &((t_vec3){(int)(pos->x + pos->y), fts[0], fts[1]}),
			sizeof(t_vec3));
		drawcasting(v, game, screendist, 1);
	}
}
