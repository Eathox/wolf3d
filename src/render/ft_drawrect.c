/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_drawrect.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 18:27:19 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:51:06 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include <math.h>

static float	calcpercentage(t_vec3 *hit, t_vec3 *corner0, t_vec3 *corner1)
{
	float val;

	val = 0;
	if (hit->x == corner0->x && hit->y != corner0->y)
	{
		if (corner0->y < corner1->y)
			val = (hit->y - corner0->y) / (corner1->y - corner0->y);
		else
			val = (hit->y - corner1->y) / (corner0->y - corner1->y);
	}
	else if (hit->x != corner0->x && hit->y == corner0->y)
	{
		if (corner0->x < corner1->x)
			val = (hit->x - corner0->x) / (corner1->x - corner0->x);
		else
			val = (hit->x - corner1->x) / (corner0->x - corner1->x);
	}
	return (val);
}

void			ft_drawrect(t_vec3 *v, float fade, t_game *game, t_hit *hit)
{
	short	x;
	short	y;
	t_vec3	p[4];

	p[0] = v[0];
	p[1] = v[1];
	x = (short)(v[0].x);
	if (v[1].x >= SCREEN_WIDTH)
		v[1].x = SCREEN_WIDTH - 1;
	if (v[1].y >= SCREEN_HEIGHT)
		v[1].y = SCREEN_HEIGHT - 1;
	while (x < (short)v[1].x)
	{
		p[3].x = calcpercentage(&(hit->pos), &(hit->line[0]), &(hit->line[1]));
		y = (short)(v[0].y);
		while (y < (short)v[1].y)
		{
			p[2] = (t_vec3){x, y, 0};
			ft_plot(x, y, ft_getcolour(fade, game, p, hit), game->img_add);
			y++;
		}
		x++;
	}
}
