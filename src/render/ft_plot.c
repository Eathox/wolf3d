/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_plot.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/05 16:36:24 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:49:09 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"

void	ft_plot(int x, int y, t_color colour, void *img_add)
{
	int *i_img;

	i_img = (int *)img_add;
	if (colour == 0xFF000000)
		return ;
	if (x < 0 || y < 0 || x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT)
		return ;
	ft_memcpy(&i_img[(y * SCREEN_WIDTH) + x], &colour, 4);
}
