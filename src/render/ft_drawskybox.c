/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_drawskybox.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/21 19:37:08 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/22 13:50:21 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static void	drawrow(t_img *skybox, t_game *game, int x)
{
	short	y;
	int		imgx;
	float	per;
	float	i;

	per = (game->angle / 360) * skybox->width;
	per += (((float)FOV / 360) * skybox->width) * ((float)x / SCREEN_WIDTH);
	imgx = (int)per;
	y = (short)SCREEN_HEIGHT - 1;
	while (y >= 0)
	{
		i = skybox->height - (skybox->height * ((float)y / SCREEN_HEIGHT) + 1);
		ft_plot(x, y, skybox->pixels[((int)i * skybox->width) + imgx],
			game->img_add);
		y--;
	}
}

static void	drawchunk(t_img *skybox, t_game *game, int start, int finish)
{
	while (start <= finish)
	{
		drawrow(skybox, game, finish);
		finish--;
	}
}

void		ft_drawskybox(t_img *skybox, t_game *game)
{
	int		x;
	int		step;

	if (skybox == NULL)
		return ;
	step = POOL_SIZE * 4;
	x = SCREEN_WIDTH / step;
	while (step > 0)
	{
		ft_poolqueback(game->pool, drawchunk, 4, skybox, game,
			x * (step - 1), (x * step) + 1);
		step--;
	}
	ft_pooljoin(game->pool);
}
