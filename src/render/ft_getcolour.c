/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_getcolour.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/09 14:32:53 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:49:30 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include <math.h>

static t_img	*getwallside(t_hit *hit, t_game *game)
{
	t_img *img;

	img = NULL;
	if (hit->line[0].x == hit->line[1].x)
	{
		if (hit->line[0].y - hit->line[1].y > 0)
			img = game->ttext[0];
		else
			img = game->ttext[1];
	}
	else if (hit->line[0].y == hit->line[1].y)
	{
		if (hit->line[0].x - hit->line[1].x > 0)
			img = game->ttext[2];
		else
			img = game->ttext[3];
	}
	return (img);
}

static t_color	applyfade(t_color colour, float fade)
{
	t_color	out;
	t_color	r;
	t_color	g;
	t_color	b;

	r = (colour & 0x00ff0000) >> 16;
	g = (colour & 0x0000ff00) >> 8;
	b = (colour & 0x000000ff);
	r *= fade;
	b *= fade;
	g *= fade;
	out = (r << 16) + (g << 8) + b;
	return (out);
}

static t_img	*getimage(t_hit *hit, t_game *game)
{
	if (hit->uid == WALL)
		return (getwallside(hit, game));
	else if (hit->uid == WALL_A)
		return (game->ttext[0]);
	else if (hit->uid == WALL_B)
		return (game->ttext[1]);
	else if (hit->uid == WALL_C)
		return (game->ttext[2]);
	else if (hit->uid == WALL_D)
		return (game->ttext[3]);
	else if (hit->uid == WALL_E)
		return (game->ttext[4]);
	return (NULL);
}

t_color			ft_getcolour(float fade, t_game *game, t_vec3 *v, t_hit *hit)
{
	t_color	colour;
	float	xdist;
	float	ydist;
	t_img	*img;

	img = getimage(hit, game);
	if (img == NULL)
		return (0);
	xdist = fabs(v[3].x) * img->width;
	ydist = (v[2].y - v[0].y) / (v[1].y - v[0].y);
	ydist = fabs(img->height * ydist);
	colour = img->pixels[(((int)ydist * img->width) +
		(int)xdist) % (img->width * img->height)];
	colour = applyfade(colour, fade);
	return (colour);
}
