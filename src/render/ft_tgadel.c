/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_tgadel.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/22 17:03:12 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:03:16 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"

void	ft_tgadel(t_img **img)
{
	if (img == NULL || *img == NULL)
		return ;
	ft_memdel((void **)&(*img)->pixels);
	free(*img);
}
