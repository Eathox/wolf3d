/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_draw3dview.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 18:30:22 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:48:44 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"
#include <math.h>

static float	calchb(t_hit *hit, t_game *game, float angle)
{
	float	dist;
	float	hb;

	hb = TILE_SPACING;
	dist = ft_dist(&(hit->pos),
		&(t_vec3){game->position.x, game->position.y, 0}) *
		cosf(angle * PI_RADIANS);
	hb /= dist;
	hb = 1 - hb;
	if (hb > 1)
		hb = 0;
	return (hb);
}

static void		fill(t_vec3 *pos, t_hit *hit, float angle, t_game *game)
{
	float	fade;
	float	hb;
	t_vec3	v[2];

	hb = calchb(hit, game, angle);
	if (hb == -100)
		return ;
	if (hb < 0)
		fade = 255;
	else
		fade = 255 - (hb * FADEINTESITY);
	fade /= 255;
	hb = hb * HALF_SCREEN_H;
	v[0] = (t_vec3){(int)pos->x, hb, 0};
	v[1] = (t_vec3){(int)(pos->x + pos->y), SCREEN_HEIGHT - hb, 0};
	ft_drawrect(v, fade, game, hit);
	ft_drawceilingfloor((float[2]){hb, angle}, game, pos, hit);
}

static void		drawchunk(t_game *game, t_hit *hits, int start, int max)
{
	float	splitx;
	float	angle;
	float	step;

	step = ((float)RAYCOUNT / FOV);
	splitx = ((float)SCREEN_WIDTH / RAYCOUNT);
	while (start <= max)
	{
		angle = (float)max / step;
		if (angle > FOV2)
			angle -= FOV2;
		else
			angle = FOV2 - angle;
		fill(&(t_vec3){((float)max * splitx), splitx, 0}, &hits[max], angle,
			game);
		max--;
	}
}

void			ft_draw3dview(t_game *game, t_hit *hits)
{
	int		x;
	int		step;

	step = POOL_SIZE * 4;
	x = RAYCOUNT / step;
	while (step > 0)
	{
		ft_poolqueback(game->pool, drawchunk, 4, game, hits, x * (step - 1),
			x * step);
		step--;
	}
	ft_pooljoin(game->pool);
}
