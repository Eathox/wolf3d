/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_readtga.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: ehollidg <ehollidg@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/01 16:13:59 by ehollidg       #+#    #+#                */
/*   Updated: 2019/08/19 14:46:18 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"
#include <unistd.h>
#include <fcntl.h>

static void		strtoimg(t_img *img, t_tga *tga, unsigned char *str)
{
	size_t i;
	size_t j;
	size_t size;
	size_t max;

	i = 19 + tga->idlen;
	if (tga->clr_map_type == 2)
		i += tga->cmlen * (tga->cmsize / 4);
	size = img->pxdepth / 8;
	img->pixels = (unsigned int*)ft_memalloc(sizeof(unsigned int) *
		(img->height * img->width));
	max = (img->height * img->width * size) + i;
	j = 0;
	while (i < max)
	{
		img->pixels[j] = ft_tgapfp(str, i, img);
		i += size;
		j++;
	}
}

static void		settgaheader(unsigned char *str, t_tga *tga, t_img *img)
{
	tga->idlen = (str[0]);
	tga->clr_map_type = (str[1]);
	tga->img_type = (str[2]);
	tga->cmpos = (str[4] << 8) | str[3];
	tga->cmlen = (str[6] << 8) | str[5];
	tga->cmsize = str[7];
	img->xorigin = (str[9] << 8) | str[8];
	img->yorigin = (str[11] << 8) | str[10];
	img->width = (str[13] << 8) | str[12];
	img->height = (str[15] << 8) | str[14];
	img->pxdepth = str[16];
}

t_img			*ft_readtga(char *file)
{
	int				fd;
	unsigned char	*str;
	t_img			*img;
	t_tga			tga;

	fd = open(file, O_RDONLY);
	if (fd == -1)
		return (NULL);
	ft_readfile(fd, (char**)&str);
	close(fd);
	if (str == NULL)
		return (NULL);
	img = (t_img *)ft_memalloc(sizeof(t_img));
	if (img == NULL)
		return (NULL);
	settgaheader(str, &tga, img);
	if (tga.img_type == 0)
	{
		ft_tgadel(&img);
		return (NULL);
	}
	strtoimg(img, &tga, str);
	free(str);
	return (img);
}
