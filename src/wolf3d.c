/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   wolf3d.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/25 13:23:06 by pholster       #+#    #+#                */
/*   Updated: 2019/08/22 13:42:20 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"
#include "libft/ft_printf.h"

static int	putcontrols(void)
{
	ft_putendl("Controls:\tw or up -> Forward");
	ft_putendl("\t\ts or down -> Backward");
	ft_putendl("\t\ta or left -> Turn Left");
	ft_putendl("\t\td or right -> Turn Right");
	ft_putendl("\t\tq -> Strafing Left");
	ft_putendl("\t\te -> Strafing Right");
	return (0);
}

int			main(int ac, char **av)
{
	t_level	*level;

	level = NULL;
	if (ac != 2)
		return (ft_puterror("./wolf3d [level_file] or -controls\n"));
	if (ft_strequ(av[1], "-controls"))
		return (putcontrols());
	ft_printf("Loading level: %s\n", av[1]);
	level = ft_levelnew(av[1]);
	if (level == NULL)
		return (ft_dprintf(2, "Invalid level: %s\n", av[1]));
	ft_printf("Succsefully loaded level: %s\n", av[1]);
	ft_game(level);
	ft_leveldel(&level);
	return (0);
}
