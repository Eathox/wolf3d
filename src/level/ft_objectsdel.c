/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_objectsdel.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 09:38:39 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:01:44 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/libft.h"

void	ft_objectsdel(t_object ***objects)
{
	size_t		i;
	t_object	*obj;

	i = 0;
	if (objects == NULL || *objects == NULL)
		return ;
	while ((*objects)[i] != NULL)
	{
		obj = (*objects)[i];
		ft_memdel((void **)&obj);
		i++;
	}
	free(*objects);
	*objects = NULL;
}
