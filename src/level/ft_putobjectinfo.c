/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_putobjectinfo.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/11 14:00:45 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:01:49 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/ft_printf.h"

void	ft_putobjectinfo(t_object *obj)
{
	if (obj == NULL)
		return ;
	if (obj->uid == SPAWN)
		ft_printf("%{MAGENTA}Uid:%{} %{GREEN}%i%{} - x: %.2f y: %.2f z: %.2f\n",
		obj->uid, obj->point.x, obj->point.y, obj->point.z);
	else
		ft_printf("%{MAGENTA}Uid:%{} %i - x: %.2f y: %.2f z: %.2f\n",
		obj->uid, obj->point.x, obj->point.y, obj->point.z);
}
