/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_puttileinfo.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/11 14:00:45 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:01:55 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/ft_printf.h"

void	ft_puttileinfo(t_tile *tile)
{
	if (tile == NULL)
		return ;
	ft_printf("%{CYAN}Uid:%{} %i\n", tile->uid);
	ft_printf("%{CYAN}point%{}[0] - x: %.2f y: %.2f z: %.2f\n",
		tile->point[0].x, tile->point[0].y, tile->point[0].z);
	ft_printf("%{CYAN}point%{}[1] - x: %.2f y: %.2f z: %.2f\n",
		tile->point[1].x, tile->point[1].y, tile->point[1].z);
	ft_printf("%{CYAN}point%{}[2] - x: %.2f y: %.2f z: %.2f\n",
		tile->point[2].x, tile->point[2].y, tile->point[2].z);
	ft_printf("%{CYAN}point%{}[3] - x: %.2f y: %.2f z: %.2f\n",
		tile->point[3].x, tile->point[3].y, tile->point[3].z);
}
