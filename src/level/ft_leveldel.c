/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_leveldel.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/26 20:57:14 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 14:25:48 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "game.h"
#include "libft/libft.h"

void	ft_leveldel(t_level **level)
{
	if (level == NULL || *level == NULL)
		return ;
	ft_terraindel(&(*level)->terrain);
	ft_objectsdel(&(*level)->objects);
	ft_strdel(&(*level)->music);
	ft_tgadel(&(*level)->skybox);
	ft_tgadel(&(*level)->ceiling);
	ft_tgadel(&(*level)->floor);
	free(*level);
	*level = NULL;
}
