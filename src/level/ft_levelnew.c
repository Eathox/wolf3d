/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_levelnew.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/25 13:37:26 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 14:29:08 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"
#include <unistd.h>
#include <fcntl.h>

static t_level	*freeret(t_level *level, int fd)
{
	if (fd != -1)
		close(fd);
	ft_leveldel(&level);
	return (NULL);
}

static t_bool	validate(t_level *level)
{
	return (level->skybox != NULL);
}

static void		finalprep(t_level *level)
{
	t_tile	*tile;
	size_t	i;

	i = 0;
	while (level->terrain[i] != NULL)
	{
		tile = level->terrain[i];
		tile->point[0].x = tile->point[0].x * TILE_SPACING;
		tile->point[0].y = tile->point[0].y * TILE_SPACING;
		tile->point[1].x = tile->point[0].x + TILE_SPACING;
		tile->point[1].y = tile->point[0].y;
		tile->point[2].x = tile->point[0].x + TILE_SPACING;
		tile->point[2].y = tile->point[0].y + TILE_SPACING;
		tile->point[3].x = tile->point[0].x;
		tile->point[3].y = tile->point[0].y + TILE_SPACING;
		i++;
	}
	level->spawn.x = level->spawn.x * TILE_SPACING + TILE_SPACING / 2;
	level->spawn.y = level->spawn.y * TILE_SPACING + TILE_SPACING / 2;
}

t_level			*ft_levelnew(char *file)
{
	t_level	*level;
	int		fd;

	if (ft_strmatch(file, "*.wolf3d") == FALSE)
		return (NULL);
	fd = open(file, O_RDONLY);
	level = (t_level *)ft_memalloc(sizeof(t_level));
	if (fd < 0 || level == NULL)
		return (freeret(level, fd));
	if (ft_readterrain(level, fd) == FALSE)
		return (freeret(level, fd));
	if (ft_readobjects(level, fd) == FALSE)
		return (freeret(level, fd));
	close(fd);
	if (level->music == NULL)
		level->music = ft_strdup(DEFAULT_MUSIC);
	if (validate(level) == FALSE)
		return (freeret(level, -1));
	finalprep(level);
	return (level);
}
