/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_terraindel.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 09:38:39 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:02:12 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/libft.h"

void	ft_terraindel(t_tile ***terrain)
{
	size_t	i;
	t_tile	*tile;

	i = 0;
	if (terrain == NULL || *terrain == NULL)
		return ;
	while ((*terrain)[i] != NULL)
	{
		tile = (*terrain)[i];
		ft_memdel((void **)&tile);
		i++;
	}
	free(*terrain);
	*terrain = NULL;
}
