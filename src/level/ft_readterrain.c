/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_readterrain.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 12:25:15 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:02:07 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/libft.h"

static size_t	freeret(char **arr, size_t y)
{
	ft_strarrdel(&arr);
	return (y);
}

static t_tile	*newtile(char *uid, size_t x, size_t y)
{
	t_tile	*tile;

	tile = (t_tile *)ft_memalloc(sizeof(t_tile));
	if (tile == NULL)
		return (NULL);
	tile->uid = ft_atoi(uid);
	if (tile->uid >= TILE_RANGE)
	{
		ft_memdel((void **)&tile);
		return (NULL);
	}
	tile->point[0].x = x;
	tile->point[0].y = y;
	return (tile);
}

static t_bool	addtolst(t_list **lst, char **arr, size_t y)
{
	static t_list	*prvlst = NULL;
	t_list			*new;
	size_t			i;

	i = 0;
	while (arr[i] != NULL)
	{
		new = ft_lstnew(NULL, 0);
		if (new == NULL)
			return (FALSE);
		new->content = newtile(arr[i], i, y);
		if (new->content == NULL)
		{
			ft_memdel((void **)&new);
			return (FALSE);
		}
		if (prvlst == NULL)
			*lst = new;
		else
			ft_lstaddbck(&prvlst, new);
		prvlst = new;
		i++;
	}
	return (TRUE);
}

static size_t	getlst(t_level *level, t_list **lst, size_t y, int fd)
{
	char			**arr;
	char			*buff;

	if (ft_getnextline(fd, &buff) < 1)
	{
		ft_strdel(&buff);
		return (y);
	}
	arr = ft_strdsplit(buff, " ");
	ft_strdel(&buff);
	if (arr == NULL)
		return (y);
	if (y == 0)
		level->xsize = ft_strarrlen((const char **)arr);
	if (ft_validateline(arr, level->xsize, "0123456789") == FALSE)
		return (freeret(arr, y));
	if (addtolst(lst, arr, y) == FALSE)
		return (freeret(arr, y));
	ft_strarrdel(&arr);
	y = getlst(level, lst, y + 1, fd);
	return (y);
}

t_bool			ft_readterrain(t_level *level, int fd)
{
	t_list	*lst;

	lst = NULL;
	level->ysize = getlst(level, &lst, 0, fd);
	if (lst == NULL)
		return (FALSE);
	level->terrain = (t_tile **)ft_lsttoarr(lst);
	ft_lstdel(&lst, NULL);
	if (level->terrain == NULL)
		return (FALSE);
	return (TRUE);
}
