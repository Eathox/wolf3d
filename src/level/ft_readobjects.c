/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_readobjects.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/06/27 12:25:15 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 14:23:45 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "game.h"
#include "libft/libft.h"

static t_object	*newobj(t_level *level, char **arr)
{
	int			x;
	int			y;
	t_object	*obj;

	if (ft_validateline(arr, 3, "0123456789") == FALSE)
		return (NULL);
	obj = (t_object *)ft_memalloc(sizeof(t_object));
	if (obj == NULL)
		return (NULL);
	obj->uid = ft_atoi(arr[0]);
	x = ft_atoi(arr[1]);
	y = ft_atoi(arr[2]);
	if (obj->uid >= OBJ_RANGE || x < 0 || (size_t)x >= level->xsize ||
		y < 0 || (size_t)y >= level->ysize)
	{
		ft_memdel((void **)&obj);
		return (NULL);
	}
	obj->point.x = x;
	obj->point.y = y;
	return (obj);
}

static t_bool	addspecial(t_level *level, char **arr)
{
	if (ft_strequ("^", arr[0]) &&
		ft_validateline(&arr[1], 2, "0123456789") == FALSE)
		return (FALSE);
	if (ft_strequ("^", arr[0]))
	{
		level->spawn.x = ft_atoi(arr[1]);
		level->spawn.y = ft_atoi(arr[2]);
		return (level->spawn.x >= 0 && level->spawn.x < level->xsize &&
			level->spawn.y >= 0 && level->spawn.y < level->ysize);
	}
	else if (ft_strequ("#", arr[0]) && level->skybox == NULL)
		return (ft_levelsetimg(arr[1], &level->skybox));
	else if (ft_strequ("C", arr[0]) && level->ceiling == NULL)
		return (ft_levelsetimg(arr[1], &level->ceiling));
	else if (ft_strequ("F", arr[0]) && level->floor == NULL)
		return (ft_levelsetimg(arr[1], &level->floor));
	else if (ft_strequ("M", arr[0]) && level->music == NULL)
	{
		level->music = ft_strdup(arr[1]);
		return (TRUE);
	}
	return (FALSE);
}

static t_bool	addtolst(t_level *level, t_list **lst, char **arr)
{
	static t_list	*prvlst = NULL;
	t_list			*new;

	new = ft_lstnew(NULL, 0);
	if (new == NULL)
		return (FALSE);
	new->content = newobj(level, arr);
	if (new->content == NULL)
	{
		ft_memdel((void **)&new);
		return (FALSE);
	}
	if (prvlst == NULL)
		*lst = new;
	else
		ft_lstaddbck(&prvlst, new);
	prvlst = new;
	return (TRUE);
}

static t_bool	getlst(t_level *level, t_list **lst, int fd)
{
	ssize_t			sepdex;
	char			**arr;
	char			*buff;

	if (ft_getnextline(fd, &buff) == -1 || buff == NULL || buff[0] == '\0')
	{
		ft_strdel(&buff);
		return (buff == NULL || buff[0] == '\0');
	}
	arr = ft_strdsplit(buff, ", ");
	sepdex = ft_chrindex(arr[0], ':');
	ft_strdel(&buff);
	if (arr == NULL || sepdex == -1 || arr[0][sepdex + 1] != '\0')
	{
		ft_strarrdel(&arr);
		return (FALSE);
	}
	arr[0][sepdex] = '\0';
	if (addtolst(level, lst, arr) == FALSE && addspecial(level, arr) == FALSE)
	{
		ft_strarrdel(&arr);
		return (FALSE);
	}
	ft_strarrdel(&arr);
	return (getlst(level, lst, fd));
}

t_bool			ft_readobjects(t_level *level, int fd)
{
	t_list	*lst;

	lst = NULL;
	if (getlst(level, &lst, fd) == FALSE)
	{
		ft_lstdel(&lst, &ft_lstdelmem);
		return (FALSE);
	}
	level->objects = (t_object **)ft_lsttoarr(lst);
	ft_lstdel(&lst, NULL);
	if (level->objects == NULL)
		return (FALSE);
	return (TRUE);
}
