/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_levelsetimg.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/19 10:49:53 by pholster       #+#    #+#                */
/*   Updated: 2019/08/19 10:51:20 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"
#include "libft/libft.h"

t_bool		ft_levelsetimg(char *file, t_img **img)
{
	if (ft_strmatch(file, "*.tga") == FALSE)
		return (FALSE);
	*img = ft_readtga(file);
	return (*img != NULL);
}
