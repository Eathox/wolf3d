/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_validateline.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/07/01 14:21:19 by pholster       #+#    #+#                */
/*   Updated: 2019/07/23 18:02:17 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft/libft.h"

t_bool	ft_validateline(char **arr, size_t len, char *chrs)
{
	size_t		i;
	size_t		j;
	long long	bytes[2];

	i = 0;
	if (arr == NULL || chrs == NULL)
		return (FALSE);
	ft_chrsetbytes(bytes, (char *)chrs);
	while (arr[i] != NULL)
	{
		j = 0;
		while (arr[i][j] != '\0')
		{
			if (ft_chrinbytes(bytes, arr[i][j]) == FALSE)
				return (FALSE);
			j++;
		}
		i++;
	}
	if (i != len)
		return (FALSE);
	return (TRUE);
}
