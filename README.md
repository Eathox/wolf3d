<h1>Basic Raycast Rendering

2D raycast project made for Codam's project **"Wolf3D"**.

![Screenshot](examplePicture1.png)
![Screenshot](examplePicture2.png)

Default Background Music :

Lost World by Darren-Curtis | https://soundcloud.com/desperate-measurez
Music promoted by https://www.free-stock-music.com
Creative Commons Attribution 3.0 Unported License
https://creativecommons.org/licenses/by/3.0/deed.en_US
