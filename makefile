# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    makefile                                           :+:    :+:             #
#                                                      +:+                     #
#    By: pholster <pholster@student.codam.nl>         +#+                      #
#                                                    +#+                       #
#    Created: 2019/01/07 20:00:45 by pholster       #+#    #+#                 #
#    Updated: 2019/08/19 10:51:47 by pholster      ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

COLOR_DEFUALT := $(shell printf "\e[39m")
COLOR_BLACK := $(shell printf "\e[38;5;0m")
COLOR_RED := $(shell printf "\e[38;5;1m")
COLOR_GREEN := $(shell printf "\e[38;5;2m")
COLOR_YELLOW := $(shell printf "\e[38;5;3m")
COLOR_BLUE := $(shell printf "\e[38;5;4m")
COLOR_MAGENTA := $(shell printf "\e[38;5;5m")
COLOR_CYAN := $(shell printf "\e[38;5;6m")
COLOR_WHITE := $(shell printf "\e[38;5;7m")
COLOR_BRIGHT_BLACK := $(shell printf "\e[38;5;8m")
COLOR_BRIGHT_RED := $(shell printf "\e[38;5;9m")
COLOR_BRIGHT_GREEN := $(shell printf "\e[38;5;10m")
COLOR_BRIGHT_YELLOW := $(shell printf "\e[38;5;11m")
COLOR_BRIGHT_BLUE := $(shell printf "\e[38;5;12m")
COLOR_BRIGHT_MAGENTA := $(shell printf "\e[38;5;13m")
COLOR_BRIGHT_CYAN := $(shell printf "\e[38;5;14m")
COLOR_BRIGHT_WHITE := $(shell printf "\e[38;5;15m")
PRINT_MIN := $(shell printf '$(COLOR_RED)[ - ]$(COLOR_DEFUALT)')
PRINT_PLUS := $(shell printf '$(COLOR_GREEN)[ + ]$(COLOR_DEFUALT)')
PRINT_EQUAL := $(shell printf '$(COLOR_BRIGHT_CYAN)[ = ]$(COLOR_DEFUALT)')

LIBFTPATH = libft
LIBFT = $(LIBFTPATH)/libft.a

NAME = wolf3d
INCLUDES = includes
HEADERS = $(NAME).h keycodes.h keycodes_linux.h keycodes_macos.h eventmasks.h \
	helddown.h mlx.h game.h
HEADERS := $(HEADERS:%=$(INCLUDES)/%)

SRCS_ENGINE = game hookcontrols eventmousedown loop eventmouseup eventkeyup \
	eventkeydown playermove doescollide
SRCS_ENGINE := $(SRCS_ENGINE:%=src/engine/ft_%.c)

SRCS_LEVEL = levelnew leveldel terraindel readterrain readobjects objectsdel \
	validateline puttileinfo putobjectinfo levelsetimg
SRCS_LEVEL := $(SRCS_LEVEL:%=src/level/ft_%.c)

SRCS_RAYCAST = dist closer getcollision traceray tracerays
SRCS_RAYCAST := $(SRCS_RAYCAST:%=src/raycast/ft_%.c)

SRCS_RENDER = plot drawrect tgapfp readtga drawskybox tgadel draw3dview \
	getcolour drawceilingfloor
SRCS_RENDER := $(SRCS_RENDER:%=src/render/ft_%.c)

SRCS_MISC = lsttoarr
SRCS_MISC := $(SRCS_MISC:%=src/misc/ft_%.c)

SRCS = src/$(NAME).c $(SRCS_ENGINE) $(SRCS_LEVEL) $(SRCS_RAYCAST) \
	$(SRCS_MISC) $(SRCS_RENDER)

SRCS := $(sort $(SRCS))
OBJS = $(SRCS:.c=.o)

MINILIBPATH = minilibx_macos
MINILIB = $(MINILIBPATH)/libmlx.a

SYNCOPTIMISE = FALSE
CCSILENT = FALSE

CCOPTIMISE = -funroll-loops -O3
CCSTRICT = -Wall -Werror -Wextra
CCFLAGS = -g $(CCSTRICT) -I$(INCLUDES) $(CCOPTIMISE)
CCFRAMEWORK = -framework OpenGL -framework AppKit
ifeq ($(SYNCOPTIMISE), TRUE)
export CCOPTIMISE
endif

# This checks if your on linux and then compiles linux minilibx
ifeq ($(shell uname -s), Linux)
MINILIBPATH = minilibx_linux
MINILIB = $(MINILIBPATH)/libmlx.a
CCFRAMEWORK = -lXext -lX11 -lm
CCFLAGS += -pthread
endif

all: $(NAME)

$(NAME): $(MINILIB) $(LIBFT) $(OBJS)
	@printf '$(PRINT_EQUAL) $(NAME): $(NAME)\n'
	@gcc $(CCFLAGS) -o $(NAME) $(SRCS) $(LIBFT) $(MINILIB) $(CCFRAMEWORK)

%.o: %.c $(HEADERS)
ifeq ($(CCSILENT), FALSE)
	@printf '$(PRINT_PLUS) $(NAME): $(shell basename $<)\n'
endif
	@gcc $(CCFLAGS) -o $@ -c $<

$(LIBFT): FORCE
	@$(MAKE) -s -e -C $(LIBFTPATH)

$(MINILIB): FORCE
	@$(MAKE) -s -i -C $(MINILIBPATH) > /dev/null

resrc:
ifneq ($(wildcard $(OBJS) $(SRCS:.c=.c~)),)
	@printf '$(PRINT_MIN) $(NAME:%.a=%): cleaning\n'
	@rm -f $(OBJS) $(SRCS:.c=.c~)
endif
	@make $(NAME)

clean:
ifneq ($(wildcard $(OBJS) $(SRCS:.c=.c~)),)
	@printf '$(PRINT_MIN) $(NAME:%.a=%): cleaning\n'
	@rm -f $(OBJS) $(SRCS:.c=.c~)
endif
	@$(MAKE) -s -C $(LIBFTPATH) clean
	@$(MAKE) -s -C $(MINILIBPATH) clean > /dev/null

fclean: clean
ifneq ($(wildcard $(NAME)),)
	@printf '$(PRINT_MIN) $(NAME:%.a=%): deleting $(NAME)\n'
	@rm -f $(NAME)
endif
	@$(MAKE) -s -C $(LIBFTPATH) fclean
#Checks if NOT on linux and then fcleans
ifneq ($(shell uname -s), Linux)
	@$(MAKE) -s -C $(MINILIBPATH) fclean > /dev/null
endif

re: fclean $(NAME)

FORCE: ;

.PHONY: clean fclean re
